# Installation
> `npm install --save @types/single-line-log`

# Summary
This package contains type definitions for single-line-log (https://github.com/freeall/single-line-log).

# Details
Files were exported from https://www.github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/single-line-log

Additional Details
 * Last updated: Mon, 26 Feb 2018 20:02:51 GMT
 * Dependencies: none
 * Global values: none

# Credits
These definitions were written by Florian Keller <https://github.com/ffflorian>.
